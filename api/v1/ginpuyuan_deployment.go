package v1

import (
	appsv1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

type GinpuyuanDeployment struct {

	// ObjectMeta 使用 GinpuyuanObjectMeta 來定義 Deployment 的 ObjectMeta
	ObjectMeta GinpuyuanObjectMeta `json:"metadata,omitempty"`

	// Spec 使用 GinpuyuanDeploymentSpec 來定義 Deployment 物件
	Spec GinpuyuanDeploymentSpec `json:"spec,omitempty"`
}

type GinpuyuanDeploymentSpec struct {

	// Replicas 定義這個 Deployment 要建立的 Pod 數量
	Replicas *int32 `json:"replicas,omitempty"`

	// Strategy 定義這個 Deployment 的更新策略
	Strategy appsv1.DeploymentStrategy `json:"strategy,omitempty"`

	// MinReadySeconds 定義這個 Deployment 新建立出來的 Pod 如果沒有任何 Container Crash 的情形發生，
	// 應準備就緒的最小秒數，預設為 0 (即 Pod 準備就緒後就可視為可用)
	MinReadySeconds int32 `json:"minReadySeconds,omitempty"`

	// RevisionHistoryLimit 定義這個 Deployment 最多保留的歷史版本數量，預設為 10
	RevisionHistoryLimit *int32 `json:"revisionHistoryLimit,omitempty"`

	// Selector 透過 GinpuuyuanBackendDeploymentLabelSelector 用來定義 Deployment 要選擇的 Pod 標籤
	Selector GinpuyuanDeploymentLabelSelector `json:"selector,omitempty"`

	// Template 透過 GinpuyuanPodTemplateSpec 用來定義 PodTemplateSpec 物件
	Template GinpuyuanPodTemplateSpec `json:"template,omitempty"`
}

type GinpuyuanDeploymentStrategy struct {

	// Type 定義這個 Deployment 的更新策略類型，預設為 RollingUpdate
	Type appsv1.DeploymentStrategyType `json:"type,omitempty"`

	// RollingUpdate 定義這個 Deployment 的滾動更新策略
	RollingUpdate GinpuyuanDeploymentRollingUpdate `json:"rollingUpdate,omitempty"`
}

type GinpuyuanDeploymentRollingUpdate struct {

	// MaxUnavailable 定義這個 Deployment 在更新時，最多可以不可用的 Pod 數量，預設為 25%
	MaxUnavailable *intstr.IntOrString `json:"maxUnavailable,omitempty"`

	// MaxSurge 定義這個 Deployment 在更新時，最多可以超出的 Pod 數量，預設為 25%
	MaxSurge *intstr.IntOrString `json:"maxSurge,omitempty"`
}

type GinpuyuanDeploymentLabelSelector struct {

	// MatchedLabels 定義這個 Deployment 要選擇的 Pod 標籤
	MatchedLabels map[string]string `json:"matchedLabels,omitempty"`
}
