package v1

import (
	corev1 "k8s.io/api/core/v1"
)

type GinpuyuanPersistentVolumeClaim struct {

	// ObjectMeta 使用 GinpuyuanObjectMeta 來定義 PersistentVolumeClaim 的 ObjectMeta
	ObjectMeta GinpuyuanObjectMeta `json:"metadata,omitempty"`

	// Spec 使用 GinpuyuanPersistentVolumeClaimSpec 來定義 PersistentVolumeClaim 物件
	Spec GinpuyuanPersistentVolumeClaimSpec `json:"spec,omitempty"`
}

type GinpuyuanPersistentVolumeClaimSpec struct {

	// Resources 定義這個 PersistentVolumeClaim 可以使用的資源
	Resources corev1.ResourceRequirements `json:"resources,omitempty"`

	// AccessModes 定義這個 PersistentVolumeClaim 的存取模式
	AccessModes []corev1.PersistentVolumeAccessMode `json:"accessModes,omitempty"`

	// VolumeMode 定義這個 PersistentVolumeClaim 的卷模式
	VolumeMode *corev1.PersistentVolumeMode `json:"volumeMode,omitempty"`

	// StorageClassName 定義這個 PersistentVolumeClaim 的 StorageClassName
	StorageClassName *string `json:"storageClassName,omitempty"`
}
