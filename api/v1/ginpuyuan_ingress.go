package v1

import (
	networkingv1 "k8s.io/api/networking/v1"
)

type GinpuyuanIngress struct {

	// ObjectMeta 使用 GinpuyuanObjectMeta 來定義 Ingress 的 ObjectMeta
	ObjectMeta GinpuyuanObjectMeta `json:"metadata,omitempty"`

	// Spec 使用 GinpuyuanIngressSpec 來定義 Ingress 物件
	Spec GinpuyuanIngressSpec `json:"spec,omitempty"`
}

type GinpuyuanIngressSpec struct {

	// IngressClassName 定義這個 Ingress 的 IngressClassName
	IngressClassName *string `json:"ingressClassName,omitempty"`

	// Rules 定義這個 Ingress 的 IngressRule
	Rules []GinpuyuanIngressRule `json:"rules,omitempty"`
}

type GinpuyuanIngressRule struct {

	// Host 定義這個 Ingress 的 Host
	Host string `json:"host,omitempty"`

	// Paths 定義這個 Ingress 的 IngressPath
	Paths []GinpuyuanIngressPath `json:"paths,omitempty"`
}

type GinpuyuanIngressPath struct {

	// Path 定義這個 Ingress 的 Path
	Path string `json:"path,omitempty"`

	// PathType 定義這個 Ingress 的 PathType
	PathType *networkingv1.PathType `json:"pathType,omitempty"`
}
