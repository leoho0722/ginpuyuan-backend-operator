package v1

type GinpuyuanObjectMeta struct {

	// Name 定義這個 Object 的名稱
	Name string `json:"name,omitempty"`

	// Namespace 定義這個 Object 的命名空間
	Namespace string `json:"namespace,omitempty"`

	// Labels 定義這個 PodTemplateSpec 的標籤
	Labels map[string]string `json:"labels,omitempty"`

	// Annotations 定義這個 Object 的 Annotations
	Annotations map[string]string `json:"annotations,omitempty"`
}
