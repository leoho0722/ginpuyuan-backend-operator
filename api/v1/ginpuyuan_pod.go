package v1

import (
	corev1 "k8s.io/api/core/v1"
)

type GinpuyuanPodTemplateSpec struct {

	// ObjectMeta 使用 GinpuyuanObjectMeta 來定義 PodTemplateSpec 的 ObjectMeta
	ObjectMeta GinpuyuanObjectMeta `json:"metadata,omitempty"`

	// Spec 使用 GinpuyuanPodSpec 來定義 Pod 物件
	Spec GinpuyuanPodSpec `json:"spec,omitempty"`
}

type GinpuyuanPodSpec struct {

	// Containers 定義這個 Pod 的 Containers
	Containers []GinpuyuanContainer `json:"containers,omitempty"`

	// Volumes 定義這個 Pod 的 Volumes
	Volumes []GinpuyuanContainerVolume `json:"volumes,omitempty"`
}

type GinpuyuanContainer struct {

	// Name 定義這個 Container 的名稱
	Name string `json:"name,omitempty"`

	// Image 定義這個 Container 的 Image
	Image GinpuyuanContainerImage `json:"image,omitempty"`

	// ContainerPort 定義這個 Container 的 ContainerPort
	ContainerPort int32 `json:"containerPort,omitempty"`

	// Args 定義這個 Container 的 Args
	Args []string `json:"args,omitempty"`

	// Resources 定義這個 Container 可以使用的資源
	Resources corev1.ResourceRequirements `json:"resources,omitempty"`

	// VolumeMounts 定義這個 Container 的 VolumeMounts
	VolumeMounts []GinpuyuanContainerVolumeMount `json:"volumeMounts,omitempty"`
}

type GinpuyuanContainerImage struct {

	// Name 定義這個 Container 的 Image 名稱
	Name string `json:"name,omitempty"`

	// Tag 定義這個 Container 的 Image Tag
	Tag string `json:"tag,omitempty"`
}

type GinpuyuanContainerVolume struct {

	// Name 定義這個 Container 的 Volume 名稱
	Name string `json:"name,omitempty"`

	// PersistentVolumeClaim 定義這個 Container 的 PersistentVolumeClaim
	PersistentVolumeClaim *corev1.PersistentVolumeClaimVolumeSource `json:"persistentVolumeClaim,omitempty"`
}

type GinpuyuanContainerVolumeMount struct {

	// Name 定義這個 Container 的 VolumeMount 名稱
	Name string `json:"name,omitempty"`

	// MountPath 定義這個 Container 的 VolumeMount 的 MountPath
	MountPath string `json:"mountPath,omitempty"`
}
