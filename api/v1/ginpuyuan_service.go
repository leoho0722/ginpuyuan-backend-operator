package v1

import (
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

type GinpuyuanService struct {

	// ObjectMeta 使用 GinpuyuanObjectMeta 來定義 Service 的 ObjectMeta
	ObjectMeta GinpuyuanObjectMeta `json:"metadata,omitempty"`

	// Spec 使用 GinpuyuanServiceSpec 來定義 Service 物件
	Spec GinpuyuanServiceSpec `json:"spec,omitempty"`
}

type GinpuyuanServiceSpec struct {

	// Type 定義這個 Service 的類型
	// Kubernetes 提供 ClusterIP、NodePort、ExternalName、LoadBalancer 四種類型
	// 預設為 ClusterIP
	Type corev1.ServiceType `json:"type,omitempty"`

	// Selector 定義這個 Service 要選擇的 Pod 標籤
	Selector map[string]string `json:"selector,omitempty"`

	// Ports 定義這個 Service 的 Ports
	Ports []GinpuyuanServicePort `json:"ports,omitempty"`
}

type GinpuyuanServicePort struct {

	// Name 定義這個 Service 的 Port 名稱
	Name string `json:"name,omitempty"`

	// Protocol 決定要以哪種網路協議來進行連線，TCP、UDP、SCTP
	// 預設為 TCP
	Protocol corev1.Protocol `json:"protocol,omitempty"`

	// Port 定義這個 Service 要用哪個 Port 號來連到 Pod
	// 通常 port 會跟 targetPort 設定為相同的 port 號
	Port int32 `json:"port,omitempty"`

	// TargetPort 定義這個 Service 要用哪個的 TargetPort 連到 Pod 對外開放的 port 號
	// 通常 port 會跟 targetPort 設定為相同的 port 號
	TargetPort intstr.IntOrString `json:"targetPort,omitempty"`

	// NodePort 定義這個 Service 的 NodePort
	// NodePort 只有在 Service 的類型為 NodePort 或 LoadBalancer 時才會生效
	// NodePort 的預設範圍為 30000~32767
	// 沒設定的話，Kubernetes 會自動指派一個範圍內的 port 號作為 NodePort
	NodePort int32 `json:"nodePort,omitempty"`
}
