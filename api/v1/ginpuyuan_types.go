/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// GinpuyuanSpec defines the desired state of Ginpuyuan
type GinpuyuanSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Deployment 透過 GinpuyuanDeployment 用來定義 Deployment 物件
	Deployment GinpuyuanDeployment `json:"deployment,omitempty"`

	// Service 透過 GinpuyuanService 用來定義 Service 物件
	Service GinpuyuanService `json:"service,omitempty"`

	// Ingress 透過 GinpuyuanIngress 用來定義 Ingress 物件
	Ingress GinpuyuanIngress `json:"ingress,omitempty"`

	// PersistentVolume 透過 GinpuyuanPersistentVolume 用來定義 PersistentVolume 物件
	PersistentVolume GinpuyuanPersistentVolume `json:"pv,omitempty"`

	// PersistentVolumeClaim 透過 GinpuyuanPersistentVolumeClaim 用來定義 PersistentVolumeClaim 物件
	PersistentVolumeClaim GinpuyuanPersistentVolumeClaim `json:"pvc,omitempty"`
}

// GinpuyuanStatus defines the observed state of Ginpuyuan
type GinpuyuanStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// Ginpuyuan is the Schema for the Ginpuyuans API
type Ginpuyuan struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   GinpuyuanSpec   `json:"spec,omitempty"`
	Status GinpuyuanStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// GinpuyuanList contains a list of Ginpuyuan
type GinpuyuanList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Ginpuyuan `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Ginpuyuan{}, &GinpuyuanList{})
}
