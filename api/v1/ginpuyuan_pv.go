package v1

import (
	corev1 "k8s.io/api/core/v1"
)

type GinpuyuanPersistentVolume struct {

	// ObjectMeta 使用 GinpuyuanObjectMeta 來定義 PersistentVolume 的 ObjectMeta
	ObjectMeta GinpuyuanObjectMeta `json:"metadata,omitempty"`

	// Spec 使用 GinpuyuanPersistentVolumeSpec 來定義 PersistentVolumeSpec 物件
	Spec GinpuyuanPersistentVolumeSpec `json:"spec,omitempty"`
}

type GinpuyuanPersistentVolumeSpec struct {

	// Capacity 定義這個 PersistentVolume 的容量大小
	Capacity corev1.ResourceList `json:"capacity,omitempty"`

	// VolumeMode 定義這個 PersistentVolume 的卷模式
	VolumeMode *corev1.PersistentVolumeMode `json:"volumeMode,omitempty"`

	// AccessModes 定義這個 PersistentVolume 的存取模式
	AccessModes []corev1.PersistentVolumeAccessMode `json:"accessModes,omitempty"`

	// StorageClassName 定義這個 PersistentVolume 的 StorageClassName
	// 若不指定，則預設為 default 儲存類型
	StorageClassName string `json:"storageClassName,omitempty"`

	// HostPath 定義這個 PersistentVolume 的 HostPath
	// 僅供單節點的 Kubernetes Cluster 使用
	HostPath *corev1.HostPathVolumeSource `json:"hostPath,omitempty"`
}
