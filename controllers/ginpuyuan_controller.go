/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"time"

	"github.com/go-logr/logr"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	networkingv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	gpybv1 "gitlab.com/leoho0722/ginpuyuan-backend-operator/api/v1"
)

// GinpuyuanReconciler reconciles a Ginpuyuan object
type GinpuyuanReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=leoho.io,resources=ginpuyuans,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=leoho.io,resources=ginpuyuans/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=leoho.io,resources=ginpuyuans/finalizers,verbs=update
//+kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=persistentVolume,verbs=get;list;watch
//+kubebuilder:rbac:groups=core,resources=persistentVolumeClaim,verbs=get;list;watch
//+kubebuilder:rbac:groups=core,resources=pods,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=networking,resources=ingress,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Ginpuyuan object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *GinpuyuanReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	_ = log.FromContext(ctx)

	log := r.Log.WithValues("Ginpuyuan", req.NamespacedName)

	// 第一步，先取得 Ginpuyuan 物件
	ginpuyuan := &gpybv1.Ginpuyuan{}
	err := r.Get(ctx, req.NamespacedName, ginpuyuan)
	if err != nil {
		if errors.IsNotFound(err) {
			log.Error(err, "Ginpuyuan resource not found!")
			return ctrl.Result{}, nil
		} else {
			log.Error(err, "Failed to get Ginpuyuan!")
			return ctrl.Result{RequeueAfter: time.Second * 5}, err
		}
	}
	log.Info("Successfully get Ginpuyuan! ", "Ginpuyuan.Name: ", ginpuyuan.Name)

	// 接著，取得 Deployment 物件
	foundDeploy := &appsv1.Deployment{}
	err = r.Get(
		ctx,
		types.NamespacedName{
			Name:      ginpuyuan.Spec.Deployment.ObjectMeta.Name,
			Namespace: ginpuyuan.Namespace,
		},
		foundDeploy,
	)
	if err != nil {
		if errors.IsNotFound(err) {
			// 如果找不到 Deployment 的話，就建立一個新的 Deployment 物件
			newDeploy := r.NewDeployment(ginpuyuan)
			log.Info(
				"Creating a new Deployment! ",
				"Deployment.Namespace: ", newDeploy.Namespace, ", ",
				"Deployment.Name: ", newDeploy.ObjectMeta.Name,
			)

			err = r.Create(ctx, newDeploy)
			if err != nil {
				log.Error(
					err,
					"Failed to create new Deployment! ",
					"Deployment.Namespace: ", newDeploy.Namespace, ", ",
					"Deployment.Name: ", newDeploy.ObjectMeta.Name,
				)
				return ctrl.Result{RequeueAfter: time.Second * 5}, err
			}

			// Deployment created successfully - return and requeue
			return ctrl.Result{Requeue: true}, nil
		} else {
			log.Error(err, "Failed to get Deployment!")
			return ctrl.Result{RequeueAfter: time.Second * 5}, err
		}
	}

	// 如果找得到 Deployment 的話，就檢查 Deployment 的 replicas 和 image 是否和 spec 一致
	// 不一樣的話，就對 Deployment 進行更新
	replicas := ginpuyuan.Spec.Deployment.Spec.Replicas
	imageName := ginpuyuan.Spec.Deployment.Spec.Template.Spec.Containers[0].Image.Name
	imageTag := ginpuyuan.Spec.Deployment.Spec.Template.Spec.Containers[0].Image.Tag
	image := imageName + ":" + imageTag
	deployNeedUpdate := false

	if foundDeploy.Spec.Replicas != replicas {
		log.Info(
			"Deployment replicas is changed, ",
			"from ", foundDeploy.Spec.Replicas, " to ", replicas,
		)
		foundDeploy.Spec.Replicas = replicas
		deployNeedUpdate = true
	}

	if (*foundDeploy).Spec.Template.Spec.Containers[0].Image != image {
		log.Info(
			"Deployment image is changed, ",
			"from ", foundDeploy.Spec.Template.Spec.Containers[0].Image, " to ", image,
		)
		foundDeploy.Spec.Template.Spec.Containers[0].Image = image
		deployNeedUpdate = true
	}

	if deployNeedUpdate {
		err = r.Update(ctx, foundDeploy)
		if err != nil {
			log.Error(err, "Failed to update Deployment!")
			return ctrl.Result{RequeueAfter: time.Second * 5}, err
		}

		// 更新 DeploymentSpec，並重新部署到 Cluster 內
		return ctrl.Result{Requeue: true}, nil
	}

	// 接著，取得 Service 物件
	foundService := &corev1.Service{}
	err = r.Get(
		ctx,
		types.NamespacedName{
			Name:      ginpuyuan.Spec.Service.ObjectMeta.Name,
			Namespace: ginpuyuan.Namespace,
		},
		foundService,
	)
	if err != nil {
		if errors.IsNotFound(err) {
			// 如果找不到 Service 的話，就建立一個新的 Service 物件
			newSvc := r.NewService(ginpuyuan)
			log.Info(
				"Creating a new Service! ",
				"Service.Namespace: ", newSvc.Namespace, ", ",
				"Service.Name: ", newSvc.ObjectMeta.Name,
			)

			err = r.Create(ctx, newSvc)
			if err != nil {
				log.Error(
					err,
					"Failed to create new Service! ",
					"Service.Namespace: ", newSvc.Namespace, ", ",
					"Service.Name: ", newSvc.ObjectMeta.Name,
				)
				return ctrl.Result{RequeueAfter: time.Second * 5}, err
			}

			return ctrl.Result{Requeue: true}, nil
		} else {
			return ctrl.Result{RequeueAfter: time.Second * 5}, err
		}
	}

	// 取得 Ingress 物件
	foundIngress := &networkingv1.Ingress{}
	err = r.Get(
		ctx,
		types.NamespacedName{
			Name:      ginpuyuan.Spec.Ingress.ObjectMeta.Name,
			Namespace: ginpuyuan.Namespace,
		},
		foundIngress,
	)
	if err != nil {
		if errors.IsNotFound(err) {
			// 如果找不到 Ingress 的話，就建立一個新的 Ingress 物件
			newIngress := r.NewIngress(ginpuyuan)
			log.Info(
				"Creating a new Service! ",
				"Service.Namespace: ", newIngress.Namespace, ", ",
				"Service.Name: ", newIngress.ObjectMeta.Name,
			)

			err = r.Create(ctx, newIngress)
			if err != nil {
				log.Error(
					err,
					"Failed to create new Ingress! ",
					"Service.Namespace: ", newIngress.Namespace, ", ",
					"Service.Name: ", newIngress.ObjectMeta.Name,
				)
				return ctrl.Result{RequeueAfter: time.Second * 5}, err
			}

			return ctrl.Result{Requeue: true}, nil
		} else {
			log.Error(err, "Failed to get Ingress!")
			return ctrl.Result{RequeueAfter: time.Second * 5}, err
		}
	}

	// 取得 PersistentVolumeClaim 物件
	foundPvc := &corev1.PersistentVolumeClaim{}
	err = r.Get(
		ctx,
		types.NamespacedName{
			Name:      ginpuyuan.Spec.PersistentVolumeClaim.ObjectMeta.Name,
			Namespace: ginpuyuan.Namespace,
		},
		foundPvc,
	)
	if err != nil {
		if errors.IsNotFound(err) {
			// 如果找不到 PersistentVolumeClaim 的話，就建立一個新的 PersistentVolumeClaim 物件
			newPvc := r.NewPersistentVolumeClaim(ginpuyuan)
			err = r.Create(ctx, newPvc)
			if err != nil {
				log.Error(
					err,
					"Failed to create new PersistentVolumeClaim! ",
					"Service.Namespace: ", newPvc.Namespace, ", ",
					"Service.Name: ", newPvc.Name,
				)
				return ctrl.Result{RequeueAfter: time.Second * 5}, err
			}

			return ctrl.Result{Requeue: true}, nil
		} else {
			log.Error(err, "Failed to get PersistentVolumeClaim!")
			return ctrl.Result{RequeueAfter: time.Second * 5}, err
		}
	}

	// 取得 PersistentVolume 物件
	foundPv := &corev1.PersistentVolume{}
	err = r.Get(
		ctx,
		types.NamespacedName{
			Name:      ginpuyuan.Spec.PersistentVolume.ObjectMeta.Name,
			Namespace: ginpuyuan.Namespace,
		},
		foundPv,
	)
	if err != nil {
		if errors.IsNotFound(err) {
			// 如果找不到 PersistentVolume 的話，就建立一個新的 PersistentVolume 物件
			newPv := r.NewPersistentVolume(ginpuyuan)
			err = r.Create(ctx, newPv)
			if err != nil {
				log.Error(
					err,
					"Failed to create new PersistentVolume! ",
					"Service.Namespace: ", newPv.Namespace, ", ",
					"Service.Name: ", newPv.ObjectMeta.Name,
				)
				return ctrl.Result{RequeueAfter: time.Second * 5}, err
			}

			return ctrl.Result{Requeue: true}, nil
		} else {
			log.Error(err, "Failed to get PersistentVolume!")
			return ctrl.Result{RequeueAfter: time.Second * 5}, err
		}
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *GinpuyuanReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&gpybv1.Ginpuyuan{}).
		Complete(r)
}

// NewDeployment 建立一個新的 Deployment 物件
func (r *GinpuyuanReconciler) NewDeployment(cr *gpybv1.Ginpuyuan) *appsv1.Deployment {
	deploy := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.Spec.Deployment.ObjectMeta.Name,
			Namespace: cr.Namespace,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas:             cr.Spec.Deployment.Spec.Replicas,
			Strategy:             cr.Spec.Deployment.Spec.Strategy,
			MinReadySeconds:      cr.Spec.Deployment.Spec.MinReadySeconds,
			RevisionHistoryLimit: cr.Spec.Deployment.Spec.RevisionHistoryLimit,
			Selector: &metav1.LabelSelector{
				MatchLabels: cr.Spec.Deployment.Spec.Selector.MatchedLabels,
			},
			Template: r.NewPodTemplateSpec(cr),
		},
	}

	ctrl.SetControllerReference(cr, deploy, r.Scheme)
	return deploy
}

// NewPodTemplateSpec 建立一個新的 PodTemplateSpec 物件
func (r *GinpuyuanReconciler) NewPodTemplateSpec(cr *gpybv1.Ginpuyuan) corev1.PodTemplateSpec {
	imageName := cr.Spec.Deployment.Spec.Template.Spec.Containers[0].Image.Name
	imageTag := cr.Spec.Deployment.Spec.Template.Spec.Containers[0].Image.Tag
	image := imageName + ":" + imageTag

	podTemplateSpec := corev1.PodTemplateSpec{
		ObjectMeta: metav1.ObjectMeta{
			Labels: cr.Spec.Deployment.Spec.Template.ObjectMeta.Labels,
		},
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				{
					Name:  cr.Spec.Deployment.Spec.Template.Spec.Containers[0].Name,
					Image: image,
					Ports: []corev1.ContainerPort{
						{
							ContainerPort: cr.Spec.Deployment.Spec.Template.Spec.Containers[0].ContainerPort,
						},
					},
					Args:      cr.Spec.Deployment.Spec.Template.Spec.Containers[0].Args,
					Resources: cr.Spec.Deployment.Spec.Template.Spec.Containers[0].Resources,
					VolumeMounts: []corev1.VolumeMount{
						{
							Name:      cr.Spec.Deployment.Spec.Template.Spec.Containers[0].VolumeMounts[0].Name,
							MountPath: cr.Spec.Deployment.Spec.Template.Spec.Containers[0].VolumeMounts[0].MountPath,
						},
					},
				},
			},
			Volumes: []corev1.Volume{
				{
					Name: cr.Spec.Deployment.Spec.Template.Spec.Volumes[0].Name,
					VolumeSource: corev1.VolumeSource{
						PersistentVolumeClaim: cr.Spec.Deployment.Spec.Template.Spec.Volumes[0].PersistentVolumeClaim,
					},
				},
			},
		},
	}

	return podTemplateSpec
}

// NewService 建立一個新的 Service 物件
func (r *GinpuyuanReconciler) NewService(cr *gpybv1.Ginpuyuan) *corev1.Service {
	svc := &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.Spec.Service.ObjectMeta.Name,
			Namespace: cr.Namespace,
		},
		Spec: corev1.ServiceSpec{
			Type:     cr.Spec.Service.Spec.Type,
			Selector: cr.Spec.Service.Spec.Selector,
			Ports: []corev1.ServicePort{
				{
					Name:       cr.Spec.Service.Spec.Ports[0].Name,
					Protocol:   cr.Spec.Service.Spec.Ports[0].Protocol,
					Port:       cr.Spec.Service.Spec.Ports[0].Port,
					TargetPort: cr.Spec.Service.Spec.Ports[0].TargetPort,
					// NodePort:   cr.Spec.Service.Spec.Ports[0].NodePort,
				},
			},
		},
	}

	ctrl.SetControllerReference(cr, svc, r.Scheme)
	return svc
}

// NewIngress 建立一個新的 Ingress 物件
func (r *GinpuyuanReconciler) NewIngress(cr *gpybv1.Ginpuyuan) *networkingv1.Ingress {
	// ingressClassName := "nginx"
	// pathType := networkingv1.PathTypePrefix

	ingress := &networkingv1.Ingress{
		ObjectMeta: metav1.ObjectMeta{
			Name:        cr.Spec.Ingress.ObjectMeta.Name,
			Namespace:   cr.Namespace,
			Annotations: cr.Spec.Ingress.ObjectMeta.Annotations,
		},
		Spec: networkingv1.IngressSpec{
			IngressClassName: cr.Spec.Ingress.Spec.IngressClassName,
			Rules: []networkingv1.IngressRule{
				{
					Host: cr.Spec.Ingress.Spec.Rules[0].Host,
					IngressRuleValue: networkingv1.IngressRuleValue{
						HTTP: &networkingv1.HTTPIngressRuleValue{
							Paths: []networkingv1.HTTPIngressPath{
								{
									Path:     cr.Spec.Ingress.Spec.Rules[0].Paths[0].Path,
									PathType: cr.Spec.Ingress.Spec.Rules[0].Paths[0].PathType,
									Backend: networkingv1.IngressBackend{
										Service: &networkingv1.IngressServiceBackend{
											Name: cr.Spec.Service.ObjectMeta.Name,
											Port: networkingv1.ServiceBackendPort{
												Number: cr.Spec.Service.Spec.Ports[0].Port,
											},
										},
									},
								},
								{
									Path:     cr.Spec.Ingress.Spec.Rules[0].Paths[1].Path,
									PathType: cr.Spec.Ingress.Spec.Rules[0].Paths[1].PathType,
									Backend: networkingv1.IngressBackend{
										Service: &networkingv1.IngressServiceBackend{
											Name: cr.Spec.Service.ObjectMeta.Name,
											Port: networkingv1.ServiceBackendPort{
												Number: cr.Spec.Service.Spec.Ports[0].Port,
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}

	ctrl.SetControllerReference(cr, ingress, r.Scheme)
	return ingress
}

// NewPersistentVolumeClaim 建立一個新的 PersistentVolumeClaim 物件
func (r *GinpuyuanReconciler) NewPersistentVolumeClaim(cr *gpybv1.Ginpuyuan) *corev1.PersistentVolumeClaim {
	pvc := &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.Spec.PersistentVolumeClaim.ObjectMeta.Name,
			Namespace: cr.Namespace,
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes:      cr.Spec.PersistentVolumeClaim.Spec.AccessModes,
			VolumeMode:       cr.Spec.PersistentVolumeClaim.Spec.VolumeMode,
			StorageClassName: cr.Spec.PersistentVolumeClaim.Spec.StorageClassName,
			Resources:        cr.Spec.PersistentVolumeClaim.Spec.Resources,
		},
	}

	return pvc
}

// NewPersistentVolume 建立一個新的 PersistentVolume 物件
func (r *GinpuyuanReconciler) NewPersistentVolume(cr *gpybv1.Ginpuyuan) *corev1.PersistentVolume {
	pv := &corev1.PersistentVolume{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.Spec.PersistentVolume.ObjectMeta.Name,
			Namespace: cr.Namespace,
		},
		Spec: corev1.PersistentVolumeSpec{
			AccessModes:      cr.Spec.PersistentVolume.Spec.AccessModes,
			VolumeMode:       cr.Spec.PersistentVolume.Spec.VolumeMode,
			StorageClassName: cr.Spec.PersistentVolume.Spec.StorageClassName,
			Capacity:         cr.Spec.PersistentVolume.Spec.Capacity,
			PersistentVolumeSource: corev1.PersistentVolumeSource{
				HostPath: cr.Spec.PersistentVolume.Spec.HostPath,
			},
		},
	}

	return pv
}
